//
//  User.swift
//  HimmelChat
//
//  Created by Admin on 01/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class UserInfo: NSObject {
    var id: String?
    var name: String?
    var email: String?
    var profileImageUrl: String?
}
