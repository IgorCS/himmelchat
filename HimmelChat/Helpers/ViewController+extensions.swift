//
//  ViewController+extensions.swift
//  HimmelChat
//
//  Created by ItsRevo on 12/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func displayImagePicker(){
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        
        present(picker, animated: true, completion: nil)
    }
}
