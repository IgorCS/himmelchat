//
//  ProfileView.swift
//  HimmelChat
//
//  Created by ItsRevo on 12/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Firebase

class ProfileView: UIView, UITextFieldDelegate {
    
    weak var delegate: ProfileViewDelegate?
    
    var nameTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.textAlignment = .center
        return tf
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_profile")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    let nameSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(r: 80, g: 101, b: 161)
        button.setTitle("Save", for: .normal)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font  = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(setSaveButtonDelegate), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.nameTextField.delegate = self
        
        addSubview(profileImageView)
        addSubview(nameTextField)
        addSubview(nameSeparatorView)
        addSubview(saveButton)
        
        setupProfileImageViewConstraints()
        setupNameTextFieldConstraints()
        setupNameSeparatorConstrains()
        setupSaveButtonConstrains()
        handleImageTapGesture()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func handleImageTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(setSelectedProfileImageViewDelegate))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        profileImageView.addGestureRecognizer(tapGesture)
    }
    
    func setupProfileImageViewConstraints() {
        profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
    }
    
    func setupNameTextFieldConstraints() {
        // need x, y width, height constraints
        nameTextField.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        nameTextField.topAnchor.constraint(equalTo: profileImageView.bottomAnchor, constant: 12).isActive = true
        nameTextField.widthAnchor.constraint(equalToConstant: 200).isActive = true
    }
    
    func setupNameSeparatorConstrains() {
        nameSeparatorView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        nameSeparatorView.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive = true
        nameSeparatorView.widthAnchor.constraint(equalTo: nameTextField.widthAnchor).isActive = true
        nameSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    func setupSaveButtonConstrains() {
        saveButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        saveButton.topAnchor.constraint(equalTo: nameSeparatorView.bottomAnchor, constant: 12).isActive = true
        saveButton.widthAnchor.constraint(equalTo: nameSeparatorView.widthAnchor).isActive = true
        saveButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setSelectedProfileImageViewDelegate() {
        delegate?.changeProfileImageView()
    }
    
    func setSaveButtonDelegate() {
        delegate?.handleSaveButton()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldDidBeginEditing(textField: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.textFieldDidEndEditing(textField: textField)
    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
}

protocol ProfileViewDelegate: class {
    func handleSaveButton()
    func changeProfileImageView()
    func textFieldDidBeginEditing(textField: UITextField)
    func textFieldDidEndEditing(textField: UITextField)
}
