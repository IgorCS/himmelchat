//
//  SettingsView.swift
//  HimmelChat
//
//  Created by Admin on 08/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Firebase

class SettingsView: UIView {
    
    weak var delegate: SettingsViewDelegate?
    
    let logoutButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(r: 80, g: 101, b: 161)
        button.setTitle("Log Out", for: .normal)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font  = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(setLogoutDelegate), for: .touchUpInside)
        return button
    }()
    
    let editProfileButton: UIButton = {
       let button = UIButton(type: .system)
        button.backgroundColor = UIColor(r: 80, g: 101, b: 161)
        button.setTitle("Edit Profile", for: .normal)
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font  = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(setHandleEditProfileDelegate), for: .touchUpInside)
       return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(logoutButton)
        addSubview(editProfileButton)
        setupLogoutButton()
        setupEditButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLogoutButton() {
        // need x, y width, height constraints
        logoutButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoutButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
        logoutButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        logoutButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupEditButton() {
        editProfileButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        editProfileButton.topAnchor.constraint(equalTo: topAnchor, constant: 90).isActive = true
        editProfileButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        editProfileButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setLogoutDelegate() {
        delegate?.handleLogout()
    }
    
    func setHandleEditProfileDelegate() {
        delegate?.handleEditProfile()
    }
}

protocol SettingsViewDelegate: class {
    func handleLogout()
    func handleEditProfile()
}
