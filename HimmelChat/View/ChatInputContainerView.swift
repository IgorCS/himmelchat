//
//  ChatInputContainerView.swift
//  HimmelChat
//
//  Created by Admin on 14/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ChatInputContainerView: UIView, UITextViewDelegate {
    
    var chatLogController: ChatLogController? {
        didSet {
                sendButton.addTarget(chatLogController, action: #selector(ChatLogController.handleSend), for: .touchUpInside)
                
                uploadImageView.addGestureRecognizer(UITapGestureRecognizer(target: chatLogController, action: #selector(ChatLogController.handleUploadTap)))
        }
    }
    
    let sendButton: UIButton = {
       let sendButton = UIButton(type: .system)
        let image = UIImage(named: "send")
        sendButton.setImage(image, for: .normal)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        return sendButton
    }()
    
    let uploadImageView: UIImageView = {
        let uploadImageView = UIImageView()
        uploadImageView.isUserInteractionEnabled = true
        uploadImageView.contentMode = UIViewContentMode.scaleAspectFit
        uploadImageView.image = UIImage(named: "plus")
        uploadImageView.tintColor = UIColor(r: 0, g: 137, b: 249)
        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
        return uploadImageView
    }()
    
    let inputTextView: UITextView = {
        let grayColor: UIColor = UIColor(r: 135, g: 141, b: 153)
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        //textView.placeholder = "Text Message"
        textView.layer.borderWidth = 1
        textView.layer.masksToBounds = true
        textView.layer.borderColor = grayColor.cgColor
        textView.layer.cornerRadius = 20
        textView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        textView.font = .systemFont(ofSize: 20)
        textView.backgroundColor = UIColor(r: 250, g: 250, b: 250)
        return textView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(uploadImageView)
        addSubview(inputTextView)
        addSubview(sendButton)
        
        setupUploadImageViewConstrains()
        setupInputTextViewConstrains()
        setupSendButtonConstrains()
    }
    
    func setupUploadImageViewConstrains() {
        uploadImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        uploadImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        uploadImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        uploadImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func setupInputTextViewConstrains() {
        self.inputTextView.leftAnchor.constraint(equalTo: uploadImageView.rightAnchor, constant: 10).isActive = true
        self.inputTextView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.inputTextView.rightAnchor.constraint(equalTo: sendButton.leftAnchor, constant: -2).isActive = true
        self.inputTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 40).isActive = true
    }
    
    func setupSendButtonConstrains() {
        sendButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -4).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: inputTextView.centerYAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


