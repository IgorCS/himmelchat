//
//  TableViewController.swift
//  HimmelChat
//
//  Created by ItsRevo on 1/8/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class TableViewController: BaseViewController {
    
    open var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    func setupTableViewConstrains() {
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
