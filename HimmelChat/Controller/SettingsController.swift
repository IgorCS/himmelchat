//
//  SettingsController.swift
//  HimmelChat
//
//  Created by Admin on 08/12/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Firebase

class SettingsController: BaseViewController, SettingsViewDelegate {
    
    var settingsView: SettingsView!
    var messagesController: MessagesController?
    let backImage = UIImage(named: "back")
   
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = "Settings"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(handleCancel))
        setUpViews()
    }
    
    func setUpViews() {
        settingsView = SettingsView()
        settingsView?.delegate = self
        settingsView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(settingsView)
    }

    override func updateViewConstraints() {
        super.updateViewConstraints()
        addLayoutConstraints()
    }
    
    func addLayoutConstraints() {
        setupScrollViewContainer()
    }
    
    func setupScrollViewContainer() {
        settingsView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        settingsView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        settingsView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        settingsView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func handleCancel() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func handleLogout() {
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
            alertTheUser(title: "A Problem Occured", message: logoutError.localizedDescription)
        }
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "UserSignedIn")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.switchRootViewController(rootViewController: RootNavigationViewConroller.init(rootViewController: LoginController()))
        
        
    }
    
    func handleEditProfile() {
        let profileViewController = ProfileViewController()
        profileViewController.settingsController = self
        navigationController?.pushViewController(profileViewController, animated: true)
    }
}
