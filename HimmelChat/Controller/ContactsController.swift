//
//  ContactsController.swift
//  HimmelChat
//
//  Created by Admin on 28/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Firebase

class ContactsController: TableViewController, UITableViewDelegate, UITableViewDataSource {
    
    let cellId = "cellId"
    var users = [UserInfo]()
    var filteredUsers = [UserInfo]()
    var newMessageController = NewMessageController()
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.tableFooterView = UIView()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        navigationItem.title = "Add Contact"
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        setupSearchController()
        fetchUser()
        setupTableViewConstrains()
    }
    
    func setupSearchController() {
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.barTintColor = UIColor(white: 0.9, alpha: 0.9)
        searchController.searchBar.placeholder = "Search Contact"
        searchController.hidesNavigationBarDuringPresentation = false
        
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func fetchUser() {
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            
            print(snapshot)
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = UserInfo()
                user.id = snapshot.key
                
                user.setValuesForKeys(dictionary)
                self.users.append(user)
                
                self.tableView.reloadData()
            }
            print("User found")
            
        }, withCancel: nil)
    }
    
    func handleCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredUsers.count
        }
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        var user = users[indexPath.row]
        
        if searchController.isActive && searchController.searchBar.text != "" {
            user = filteredUsers[indexPath.row]
        } else {
            user = users[indexPath.row]
        }
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
        
        if let profileImageUrl = user.profileImageUrl {
            
            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            
            let user = self.users[indexPath.row]
            self.addContact(user: user)
        }
    }
    
    func filterRowsForSearchedText(_ searchText: String) {
        filteredUsers = users.filter({( user : UserInfo) -> Bool in
            return (user.name?.lowercased().contains(searchText.lowercased()))!
        })
        tableView.reloadData()
    }
    
    private func addContact(user: UserInfo) {
        let toId = user.id!
        let fromId = Auth.auth().currentUser!.uid
        let userContactRef = Database.database().reference().child("contacts").child(fromId)
        let contactFromId = Database.database().reference().child("contacts").child(fromId).child(toId)
        var count = 1
        
        userContactRef.updateChildValues([contactFromId.key: count]) {(error, ref) in
            if error != nil {
                print(error as Any)
                self.alertTheUser(title: "A problem occured", message: (error?.localizedDescription)!)
                return
            }
            count += 1
            let recipientUserContactRef = Database.database().reference().child("contacts").child(toId)
            let contactToId = Database.database().reference().child("contacts").child(toId).child(fromId)
            recipientUserContactRef.updateChildValues([contactToId.key: count])
        }
        count += 1
    }
}


extension ContactsController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let term = searchController.searchBar.text {
            filterRowsForSearchedText(term)
        }
    }
}
