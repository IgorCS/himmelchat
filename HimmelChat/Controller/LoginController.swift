//
//  LoginController.swift
//  HimmelChat
//
//  Created by Admin on 31/10/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

typealias LoginHandler = (_ msg: String?) -> Void

class LoginController: ScrollViewController, LoginContainerViewDelegate, GIDSignInUIDelegate {
    
    let messagesController = MessagesController()
    var loginHandler: LoginHandler?
    var loginContainerView: LoginContainerView!
    
    let googleButton = GIDSignInButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 135, g: 206, b: 250)
        self.view.needsUpdateConstraints()
        self.navigationController?.navigationBar.isHidden = true
        setUpViews()
        setupScrollViewContainer()
        handleLoginRegisterChange()
        loginContainerView.addSubview(googleButton)
        setupGoogleButtonConstrains()
        
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    func setupGoogleButtonConstrains() {
        googleButton.translatesAutoresizingMaskIntoConstraints = false
        googleButton.bottomAnchor.constraint(equalTo: loginContainerView.bottomAnchor, constant: -40).isActive = true
        googleButton.centerXAnchor.constraint(equalTo: loginContainerView.centerXAnchor).isActive = true
        googleButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        googleButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func setUpViews() {
        loginContainerView = LoginContainerView()
        loginContainerView.delegate = self
        loginContainerView.translatesAutoresizingMaskIntoConstraints = false
        scrollView = UIScrollView()
        self.scrollView?.addSubview(loginContainerView)
        view.addSubview(self.scrollView)
        self.hideKeyboardWhenTappedAround()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        addLayoutConstraints()
    }
    
    func addLayoutConstraints() {
        setupScrollViewContainer()
        setupLoginContainerView()
    }
    
    func setupLoginContainerView() {
        loginContainerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive  = true
        loginContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive  = true
        loginContainerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive  = true
        loginContainerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive  = true
        loginContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        let heightConstraint =  loginContainerView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        heightConstraint.priority = 250
        heightConstraint.isActive = true
    }
    
    override func viewWillLayoutSubviews(){
        scrollView.contentSize = loginContainerView.bounds.size
        super.viewWillLayoutSubviews()
    }
    
    
    func textFieldDidEndEditing(textField: UITextField){
        activeTextField = nil
    }
    
    func textFieldDidBeginEditing(textField: UITextField){
        activeTextField = textField
    }
    
    func handleSelectedProfileImageView() {
        self.displayImagePicker()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            loginContainerView.profileImageView.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleLoginRegister() {
        if loginContainerView.loginRegisterSegmentedControl.selectedSegmentIndex == 0 {
            handleLogin()
        } else {
            handleRegister()
        }
    }
    
    func handleLogin() {
        guard let email = loginContainerView.emailTextField.text, let password = loginContainerView.passwordTextField.text else {
            print("Form is not valid")
            return
        }
        
        if isInputFieldValid() {
            Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                
                if error != nil {
                    print(error ?? "A problem occured")
                    self.alertTheUser(title: "A problem occured", message: (error?.localizedDescription)!)
                    return
                } else {
                    self.loginHandler?(nil)
                }
                let defaults = UserDefaults.standard
                defaults.set(true, forKey: "UserSignedIn")
                self.navigationController?.pushViewController(self.messagesController, animated: true)
                self.dismiss(animated: true, completion: nil)
            })
        } else {
            self.alertTheUser(title: "Error", message: "Invalid Email Address. Please provide a real Email Address")
        }
    }
    
    func handleRegister() {
        guard let email = loginContainerView.emailTextField.text, let password = loginContainerView.passwordTextField.text, let name = loginContainerView.nameTextField.text else {
            print("Form is not valid")
            return
        }
        if isInputFieldValid() {
            Auth.auth().createUser(withEmail: email, password: password, completion: { (user: User?, error) in
                
                if error != nil {
                    print(error ?? "A problem occured")
                    self.alertTheUser(title: "A problem occured", message: (error?.localizedDescription)!)
                    return
                }
                guard let uid = user?.uid else {
                    return
                }
                //successful authenticated user
                self.handleSuccessfulAuthentification(uid: uid, name: name, email: email)
            })
        } else {
            self.alertTheUser(title: "Error", message: "Invalid Email Address. Please provide a real Email Address")
        }
    }
    
    private func handleSuccessfulAuthentification(uid: String, name: String, email: String) {
        let storageRef = Storage.storage().reference().child("profile_images").child("\(uid)")
        if let profileImage = loginContainerView.profileImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
            
            storageRef.putData(uploadData, metadata: nil, completion: {
                (metadata, error) in
                if error != nil {
                    print(error ?? "image not load")
                    return
                }
                if let profileImageUrl = metadata?.downloadURL()?.absoluteString {
                    let values = ["name": name, "email": email, "profileImageUrl": profileImageUrl]
                    self.registerUserIntoDatabaseWithUID(uid: uid, values: values as [String : AnyObject])
                }
            })
        }
    }
    
    private func registerUserIntoDatabaseWithUID(uid: String, values: [String: AnyObject]) {
        let ref = Database.database().reference()
        
        let usersReferece = ref.child("users").child(uid)
        usersReferece.updateChildValues(values, withCompletionBlock: { (err, ref) in
            
            if err != nil {
                print(err ?? "User was not saved in Firebase db")
                self.alertTheUser(title: "A Problem Occured", message: (err?.localizedDescription)!)
                return
            }
            self.pushToMessagesController(values: values)
        })
    }
    
    private func pushToMessagesController(values: [String: AnyObject]) {
        let user = UserInfo()
        user.setValuesForKeys(values)
        self.messagesController.setupNavBarWithUser(user: user)
        self.messagesController.navigationItem.title = values["name"] as? String
        self.navigationController?.pushViewController(self.messagesController, animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleLoginRegisterChange() {
        let title = loginContainerView.loginRegisterSegmentedControl.titleForSegment(at: loginContainerView.loginRegisterSegmentedControl.selectedSegmentIndex)
        loginContainerView.loginRegisterButton.setTitle(title, for: .normal)
        
        //change height of inputContainerView
        loginContainerView.inputsContainerViewHeightAnchor?.constant = loginContainerView.loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 100 : 150
        
        changeNameTextFieldHeight()
        changeEmailTextFieldHeight()
        changePasswordTextFieldHeight()
        handleImageChange()
    }
    
    func changeNameTextFieldHeight() {
        loginContainerView.nameTextFieldHeightAnchor?.isActive = false
        loginContainerView.nameTextFieldHeightAnchor = loginContainerView.nameTextField.heightAnchor.constraint(equalTo: loginContainerView.inputsContainerView.heightAnchor, multiplier: loginContainerView.loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1/3)
        loginContainerView.nameTextFieldHeightAnchor?.isActive = true
        loginContainerView.nameTextField.layer.masksToBounds = true
    }
    
    func changeEmailTextFieldHeight() {
        loginContainerView.emailTextFieldHeightAnchor?.isActive = false
        loginContainerView.emailTextFieldHeightAnchor = loginContainerView.emailTextField.heightAnchor.constraint(equalTo: loginContainerView.inputsContainerView.heightAnchor, multiplier: loginContainerView.loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        loginContainerView.emailTextFieldHeightAnchor?.isActive = true
    }
    
    func changePasswordTextFieldHeight() {
        loginContainerView.passwordTextFieldHeightAnchor?.isActive = false
        loginContainerView.passwordTextFieldHeightAnchor = loginContainerView.passwordTextField.heightAnchor.constraint(equalTo: loginContainerView.inputsContainerView.heightAnchor, multiplier: loginContainerView.loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        loginContainerView.passwordTextFieldHeightAnchor?.isActive = true
    }
    
    func handleImageChange() {
        if loginContainerView.loginRegisterSegmentedControl.selectedSegmentIndex == 1 {
            loginContainerView.profileImageView.image = UIImage(named: "add_profile")
            loginContainerView.profileImageView.tintColor = UIColor(r: 0, g: 137, b: 249)
        } else {
            loginContainerView.profileImageView.image = UIImage(named: "logo1")
        }
    }
    
    func isValidEmailAddress(_ email: String) -> Bool {
        
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func isInputFieldValid() -> Bool{
        
        let providedEmailAddress = loginContainerView.emailTextField.text
        let isEmailAddressValid = isValidEmailAddress(providedEmailAddress!)
        
        if loginContainerView.emailTextField.text != "" && loginContainerView.passwordTextField.text != "" && isEmailAddressValid {
            return true
        }
        return false
    }
}

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
