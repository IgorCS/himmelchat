//
//  NewMessageController.swift
//  HimmelChat
//
//  Created by Admin on 01/11/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Firebase

class NewMessageController: TableViewController, UITableViewDelegate, UITableViewDataSource {

    let cellId = "cellId"
    var users = [UserInfo]()
    var filteredUsers = [UserInfo]()
    var messagesController: MessagesController?
    
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    
    let searchController = UISearchController(searchResultsController: nil)
    
    let contactImage = UIImage(named: "contacts1")
    let backImage = UIImage(named: "back")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(handleCancel))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: contactImage, style: .plain, target: self, action: #selector(handleAddContact))
        navigationItem.title = "New Chat"
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        setupSearchController()
        fetchContacts()
        tableView.allowsSelectionDuringEditing = true
        setupTableViewConstrains()
    }
    
    func setupSearchController() {
        definesPresentationContext = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.barTintColor = UIColor(white: 0.9, alpha: 0.9)
        searchController.searchBar.placeholder = "Search"
        searchController.hidesNavigationBarDuringPresentation = false
        
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func handleAddContact() {
        let addContactsController = ContactsController()
        addContactsController.newMessageController = self
        let navController = UINavigationController(rootViewController: addContactsController)
        present(navController, animated: true, completion: nil)
    }
    
    func fetchContacts() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let contactRef = Database.database().reference().child("contacts").child(uid)
        contactRef.observe(.childAdded, with: { (snapshot) in
            
            let userId = snapshot.key
            let userRef = Database.database().reference().child("users").child(userId)
            userRef.observe(.value, with: { (snapshot) in
                guard let dictionary = snapshot.value as? [String: AnyObject] else {
                    return
                }
                print(snapshot)
                
                let user = UserInfo()
                user.id = snapshot.key
                user.setValuesForKeys(dictionary)
                self.users.append(user)
                self.tableView.reloadData()
                print("User found")
                
            }, withCancel: nil)
        }, withCancel: nil)
    }
    
    func handleCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredUsers.count
        }
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        var user = users[indexPath.row]
        
        if searchController.isActive && searchController.searchBar.text != "" {
            user = filteredUsers[indexPath.row]
        } else {
            user = users[indexPath.row]
        }
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
        
        if let profileImageUrl = user.profileImageUrl {
            
            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            
            let user = self.users[indexPath.row]
            self.messagesController?.showChatControllerForUser(user: user)
        }
    }
    
    func filterRowsForSearchedText(_ searchText: String) {
        filteredUsers = users.filter({( user : UserInfo) -> Bool in
            return (user.name?.lowercased().contains(searchText.lowercased()))!
        })
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let user = self.users[indexPath.row]
        deleteContact(user: user, indexPath: indexPath.row)
    }
    
    private func deleteContact(user: UserInfo, indexPath: Int) {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        let toId = user.id
        if let contactId = toId {
            Database.database().reference().child("contacts").child(uid).child(contactId).removeValue(completionBlock: { (error, ref) in
                if error != nil {
                    print("Failed to delete the contact", error!)
                    self.alertTheUser(title: "A problem occured", message: (error?.localizedDescription)!)
                    return
                }
                self.users.remove(at: indexPath)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
        }
    }
}


extension NewMessageController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let term = searchController.searchBar.text {
            filterRowsForSearchedText(term)
        }
    }
}
