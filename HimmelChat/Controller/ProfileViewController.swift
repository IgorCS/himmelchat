//
//  ProfileViewController.swift
//  HimmelChat
//
//  Created by ItsRevo on 12/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: ScrollViewController, ProfileViewDelegate {
    
    var settingsController: SettingsController?
    var loginController: LoginController!
    var profileView: ProfileView!
    let backImage = UIImage(named: "back")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = "Profile"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(handleCancel))
        self.view.needsUpdateConstraints()
        setUpViews()
        setupScrollViewContainer()
        fetchUsers()
        self.hideKeyboardWhenTappedAround()
    }
    
    func setUpViews() {
        profileView = ProfileView()
        profileView?.delegate = self
        profileView.translatesAutoresizingMaskIntoConstraints = false
        scrollView = UIScrollView()
        self.scrollView?.addSubview(profileView)
        view.addSubview(self.scrollView)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        addLayoutConstraints()
    }
    
    func addLayoutConstraints() {
        setupScrollViewContainer()
        setupProfileViewContainer()
    }
    
    func setupProfileViewContainer() {
        profileView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive  = true
        profileView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive  = true
        profileView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive  = true
        profileView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive  = true
        profileView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        let heightConstraint =  profileView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        heightConstraint.priority = 250
        heightConstraint.isActive = true
    }
    
    func handleCancel() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func changeProfileImageView() {
        self.displayImagePicker()
    }
    
    func fetchUsers() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            print(snapshot)
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = UserInfo()
                user.setValuesForKeys(dictionary)
                self.setupProfileImageWithUser(user: user)
            }
        }, withCancel: nil)
    }
    
    func setupProfileImageWithUser(user: UserInfo) {
        if let profileImageUrl = user.profileImageUrl {
            profileView.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        profileView.nameTextField.text = user.name
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            profileView.profileImageView.image = selectedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func handleSaveButton() {
        guard let userId = Auth.auth().currentUser?.uid, let name = profileView.nameTextField.text else {
            print("Form is not valid")
            return
        }
        Auth.auth().currentUser?.createProfileChangeRequest().commitChanges(completion: { (error) in
            if error != nil {
                print(error ?? "A problem occured")
                self.alertTheUser(title: "A problem occured", message: (error?.localizedDescription)!)
                return
            }
            self.handleSuccessfullProfileChanges(uid: userId, name: name)
        })
    }
    
    private func handleSuccessfullProfileChanges(uid: String, name: String) {
        guard let userId = Auth.auth().currentUser?.uid else {
            return
        }
        let storageRef = Storage.storage().reference().child("profile_images").child("\(userId)")
        
        if let profileImage = profileView.profileImageView.image, let uploadData = UIImageJPEGRepresentation(profileImage, 0.1) {
            
            storageRef.putData(uploadData, metadata: nil, completion: {
                (metadata, error) in
                
                if error != nil {
                    print(error ?? "image not load")
                    self.alertTheUser(title: "A problem occured", message: (error?.localizedDescription)!)
                    return
                }

                if let profileImageUrl = metadata?.downloadURL()?.absoluteString {
                    let values = ["name": name, "profileImageUrl": profileImageUrl]
                    self.saveUserChangesIntoDatabase(uid: uid, values: values as [String : AnyObject])
                }
            })
        }
    }
    
    private func saveUserChangesIntoDatabase(uid: String, values: [String: AnyObject]) {
        let ref = Database.database().reference()
        let usersReferece = ref.child("users").child(uid)
        usersReferece.updateChildValues(values, withCompletionBlock: { (err, ref) in
            
            if err != nil {
                print(err ?? "User was not saved in Firebase db")
                self.alertTheUser(title: "A problem occured", message: (err?.localizedDescription)!)
                return
            }
            let user = UserInfo()
            user.setValuesForKeys(values)
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    override func viewWillLayoutSubviews(){
        scrollView.contentSize = profileView.bounds.size
        super.viewWillLayoutSubviews()
    }
    
    
    func textFieldDidEndEditing(textField: UITextField){
        activeTextField = nil
    }
    
    func textFieldDidBeginEditing(textField: UITextField){
        activeTextField = textField
    }
}
