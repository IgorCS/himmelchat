//
//  BaseViewController.swift
//  HimmelChat
//
//  Created by ItsRevo on 12/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class BaseViewController: UIViewController{
    
    let reachability = Reachability()!
    
    let banner : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 255, g: 56, b: 56)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let internetStatusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "NO INTERNET CONNECTION"
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        reachability.whenReachable = { _ in
            DispatchQueue.main.async {
                print("Connected to internet")
                self.banner.alpha = 0
            }
        }
        
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                print("NOT Connected to internet")
                self.addBannerToView()
                self.banner.alpha = 1
                self.banner.layer.zPosition = 1
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch {
            print("could not connect to network")
        }
    }
    
    func addBannerToView() {
        self.view.addSubview(self.banner)
        self.setupBannerConstrains()
    }
    
    func internetChanged(note: Notification) {
        let reachability = note.object as! Reachability
        if reachability.connection != .none {
            if reachability.connection == .wifi {
                DispatchQueue.main.async {
                    print("wifi internet")
                    self.banner.alpha = 0

                }
            }else if reachability.connection == .cellular{
                DispatchQueue.main.async {
                    print("cellular net")
                    self.banner.alpha = 0
                }
            }
        }else {
            DispatchQueue.main.async {
                print("NO NET")
                self.addBannerToView()
                self.banner.alpha = 1
                self.banner.layer.zPosition = 1
            }
        }
    }
    
    func setupBannerConstrains() {
        banner.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        banner.heightAnchor.constraint(equalToConstant: 60).isActive = true
        banner.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        banner.addSubview(internetStatusLabel)
        setupInternetStatusLabelConstrains()
    }
    
    func setupInternetStatusLabelConstrains() {
        internetStatusLabel.centerXAnchor.constraint(equalTo: banner.centerXAnchor).isActive = true
        internetStatusLabel.centerYAnchor.constraint(equalTo: banner.centerYAnchor).isActive = true
    }
    
    func alertTheUser(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion:nil)
    }

}
